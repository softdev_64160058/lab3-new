/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.sarunpat.lab3new;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author USER
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow1By_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentplayer = "O";
        assertEquals(true, OXProgram.checkRow(table, currentplayer));
    }

    public void testCheckWinRow2By_X_output_true() {
        String[][] table = {{"O", "O", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String currentplayer = "X";
        assertEquals(true, OXProgram.checkRow(table, currentplayer));
    }

    public void testCheckWinRow3By_O_output_true() {
        String[][] table = {{"-", "X", "-"}, {"-", "X", "-"}, {"O", "O", "O"}};
        String currentplayer = "O";
        assertEquals(true, OXProgram.checkRow(table, currentplayer));
    }

    //--------------------------------------------------------------------------------------------------------------
    public void testCheckWinCol1By_X_output_true() {
        String[][] table = {{"X", "O", "-"}, {"X", "-", "-"}, {"X", "O", "-"}};
        String currentplayer = "X";
        assertEquals(true, OXProgram.checkCol(table, currentplayer));
    }

    public void testCheckWinCol2By_O_output_true() {
        String[][] table = {{"X", "O", "-"}, {"-", "O", "-"}, {"X", "O", "-"}};
        String currentplayer = "O";
        assertEquals(true, OXProgram.checkCol(table, currentplayer));
    }

    public void testCheckWinCol3By_X_output_true() {
        String[][] table = {{"-", "-", "X"}, {"-", "O", "X"}, {"-", "O", "X"}};
        String currentplayer = "X";
        assertEquals(true, OXProgram.checkCol(table, currentplayer));
    }

    //--------------------------------------------------------------------------------------------------------------
    public void testCheckWinZRBy_O_output_true() {
        String[][] table = {{"O", "-", "-"}, {"-", "O", "X"}, {"X", "-", "O"}};
        String currentplayer = "O";
        assertEquals(true, OXProgram.checkZR(table, currentplayer));
    }

    //--------------------------------------------------------------------------------------------------------------
   //Test checkWinZ by X output True is HERE...
    public void testCheckWinZBy_X_output_true() {
        String[][] table = {{"-", "-", "X"}, {"O", "X", "-"}, {"X", "-", "O"}};
        String currentplayer = "X";
        assertEquals(true, OXProgram.checkZ(table, currentplayer));
    }//Test checkWinZ by X output True is HERE...

    //--------------------------------------------------------------------------------------------------------------
    public void testCheckDrawBy_O_output_False() {
        String[][] table = {{"X", "X", "O"}, {"O", "X", "O"}, {"X", "O", "O"}};
        String currentplayer = "O";
        assertEquals(false, OXProgram.checkDraw(table, currentplayer));
    }

    public void testCheckDrawBy_X_output_True() {
        String[][] table = {{"O", "X", "X"}, {"X", "O", "O"}, {"X", "O", "X"}};
        String currentplayer = "O";
        assertEquals(true, OXProgram.checkDraw(table, currentplayer));
    }
}
