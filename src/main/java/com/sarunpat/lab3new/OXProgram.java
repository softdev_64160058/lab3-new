/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sarunpat.lab3new;

/**
 *
 * @author USER
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {
        return checkRow(table, currentPlayer) || checkCol(table, currentPlayer) || checkZR(table, currentPlayer) || checkZ(table, currentPlayer);
    }

    //--------------------------------------------------------------------------------------------------------------
    static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    //--------------------------------------------------------------------------------------------------------------
    static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }

    //--------------------------------------------------------------------------------------------------------------
    static boolean checkZR(String[][] table, String currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }

    //--------------------------------------------------------------------------------------------------------------
    static boolean checkZ(String[][] table, String currentPlayer) {
        return table[2][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[0][2].equals(currentPlayer);
    }

    //--------------------------------------------------------------------------------------------------------------
    static boolean checkDraw(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer) || checkCol(table, currentPlayer) || checkZR(table, currentPlayer) || checkZ(table, currentPlayer)) {
            return false;
        } else {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if ("-".equals(table[i][j])) {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
